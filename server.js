var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')
const log = require('./lib/logger')

var app = express()

var games = {}
var users = {}
var userIdBySocketId = {}

const http = require('http').Server(app)
const io = require('socket.io')(http)
const bodyParser = require('body-parser')

const port = 8060

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

// Serves index.html as all other routes
app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'), function (err) {
    if (err) {
      res.status(500).send(err)
    }
  })
})

// MATTHEW STILL NEEDS
// Takes the gameid as input
// app.post('/startGame', (req, res) => {
//   var user = users[req.body.userId]
//   if (req.session.owner == req.body.gameId) {
//     let gameId = req.body.gameId
//     let game = games[gameId]

//     game.timeout = setTimeout(endGame, game.time * 1000, gameid)
//     let users = []
//     for (let each in game.users) {
//       users.push(each)
//     }
//   }
// })

const createGame = require('./api/Server/CreateGame')
const getNewRandomId = require('./lib/generateRandomId')
io.on('connection', function (socket) {
  log.info('New Client Connected')

  // Example for sending data to front end.
  socket.on('subscribeToTimer', (interval) => {
    setInterval(() => {
      socket.emit('timer', new Date())
    }, interval)
  })

  socket.on('joinGame', (data) => {
    log.info('Join Game')
    var user = getUser(data, socket)
    if (games[data.gameId] != undefined || games[user.gameId] != undefined) {
      socket.join(data.gameId || user.gameId)
      socket.emit('gameJoined', user.userId)
      updateLobby(data.gameId || user.gameId, io)
    } else {
      socket.emit('problem', 'Game not found')
    }
  })

  socket.on('leaveGame', (data) => {
    log.info('Leave Game')
    let user = users[data.userId]
    delete users[data.userId]
    socket.leave(user.gameId)
    socket.emit('leaveGame', 'Left Game')
    updateLobby(user.gameId, io)
  })

  socket.on('createGame', (data) => {
    let gameId = createGame(data, games, users)
    var user = getUser(data, socket)
    socket.join(data.gameId || user.gameId)
    socket.emit('gameCreated', gameId)
  })

  socket.on('startGame', (data) => {
    log.info('Start Game')
  })
  socket.on('disconnect', () => {
    console.log('Client disconnected')
  })
  socket.on('vote', (data) => {
    let userId = userIdBySocketId[socket.id]
    let user = users[userId]
    games[user.gameId].votes[userId] = data
    socket.emit('voted', true)
    updateLobby(user.gameId, io)
  })
})

// Sets socket.io to listen on specified port
io.listen(port)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  // res.sendFile('./404.html')
})

function getUser (data, socket) {
  if (data.userId) {
    user = users[data.userId]
  } else {
    user = {
      username: data.usernames,
      userId: getNewRandomId(),
      gameId: data.gameId
    }
    users[data.userId] = user
  }
  userIdBySocketId[socket.id] = user.userId
  return user
}

function updateLobby (gameId, io) {
  console.log(games[gameId])
  io.to(gameId).emit('gameStateUpdate', games[gameId])
}

function endtimer (gameid) {
  console.log(gameid)
}

module.exports = app

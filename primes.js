var target = 100000000;
var primes = [2];
var start = process.hrtime();
for (var i = 3; i < target; i++) {
    var prime = true;
    for (var j of primes) {
        if (j > Math.ceil(Math.sqrt(i))) {
            prime = true
            break;
        }
        if (i % j == 0) {
            
            prime = false
            break;
        }
        prime = true        
    }
    if (prime) {
        primes.push(i)
    }
}
var end = process.hrtime()
const startnanoseconds = (start[0] * 1e9) + start[1];
const endnanoseconds = (end[0] * 1e9) + end[1];
var time = (endnanoseconds - startnanoseconds) / 1e6
// console.log(primes)
console.log(time)
import { socket } from './socket'

/**
 * Unsubscribes given events
 * @param {array of strings} events
 */
function unsubscribe (events = []) {
  events.forEach(function (event) {
    socket.removeAllListeners(event)
  })
}

export { unsubscribe }

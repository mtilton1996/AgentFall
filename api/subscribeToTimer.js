import { socket } from './socket'

/**
 * Sets subscriber for a timer message from socket.io
 * @param {function} callback
 */
function subscribeToTimer (callback) {
  socket.on('timer', timestamp => callback(timestamp))
  socket.emit('subscribeToTimer', 1000)
}

export { subscribeToTimer }

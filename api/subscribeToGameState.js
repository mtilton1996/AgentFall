import { socket } from './socket'

/**
 * Sets subscriber for a game update message from socket.io
 * @param {function} callback
 */
function subscribeToGameState (callback) {
  socket.on('gameStateUpdate', (data) => {
    let returnedData = {}
    console.log(data)
    callback(returnedData)
  })
}

export { subscribeToGameState }

import { socket } from './socket'

/**
 * Sends data to create game
 * @param {object} data
 */
function joinGame (data) {
  socket.emit('joinGame', data)
}

export { joinGame }

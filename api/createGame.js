import { socket } from './socket'

/**
 * Sends data to create game
 * @param {object} data
 */
function createGame (data) {
  socket.emit('createGame', data)
}

export { createGame }

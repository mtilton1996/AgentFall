const getNewRandomId = require('../../lib/generateRandomId.js')
const log = require('../../lib/logger.js')

var createGame = function (data, games, users) {
  log.info('Create Game')
  let time = data.time
  let GameName = data.GameName
  let Themes = data.Themes
  let gameId = getNewRandomId()

  let newGame = {
    time: time,
    GameName: GameName,
    Themes: Themes,
    started: false,
    ended: false,
    gameId: gameId,
    votes: {}
  }

  games[gameId] = newGame
  return gameId
}

module.exports = createGame

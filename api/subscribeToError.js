import { socket } from './socket'

/**
 * Sets subscriber for a error messages from socket.io
 * @param {function} callback
 */
function subscribeToError (callback) {
  socket.on('problem', (error) => {
    callback(error)
  })
}

export { subscribeToError }

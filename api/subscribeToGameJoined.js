import { socket } from './socket'

/**
 * Sets subscriber for a gameJoined message from socket.io
 * @param {function} callback
 */
function subscribeToGameJoined (callback) {
  socket.on('gameJoined', (userID) => {
    callback(userID)
  })
}

export { subscribeToGameJoined }

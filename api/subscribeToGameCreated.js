import { socket } from './socket'

/**
 * Sets subscriber for a gameCreated message from socket.io
 * @param {function} callback
 */
function subscribeToGameCreated (callback) {
  socket.on('gameCreated', (gameID) => {
    callback(gameID)
  })
}

export { subscribeToGameCreated }

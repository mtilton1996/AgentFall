import React, { Component } from 'react'
import {Header, Button, Divider, Card as SemanticCard, Label, Segment, Container} from 'semantic-ui-react'
import { withRouter, Redirect } from 'react-router-dom'
import Card from './Card'
import { subscribeToGameState } from '../../api/subscribeToGameState'
import { unsubscribe } from '../../api/unsubscribe'

class Lobby extends Component {
  constructor (props) {
    super(props)
    this.handleStart = this.handleStart.bind(this)
    this.handleLeave = this.handleLeave.bind(this)
    this.state = {
      gameName: '',
      people: [
        {name: 'John'},
        {name: 'Jenkins'},
        {name: 'Jake'},
        {name: 'Jim'}
      ],
      readyToPlay: false,
      leaveGame: false
    }
  }

  componentDidMount () {
    subscribeToGameState((data) => {
      localStorage.setItem('gameState', data)
    })
  }

  componentWillUnmount () {
    unsubscribe(['gameUpdate'])
  }

  handleLeave () {
    // TODO Send stuff to server to tell everyone else to leave game
    this.setState({leaveGame: true})
  }

  handleStart () {
    // TODO Send stuff to server to tell everyone else to start game
    this.setState({readyToPlay: true})
  }

  render () {
    let people = this.state.people.map(function (person, i) {
      return <Card {...person} key={person.name} />
    })
    let game = JSON.parse(localStorage.getItem('game'))

    if (this.state.readyToPlay) {
      return <Redirect push to={'/Agentfall/game/' + game.ID} />
    } else if (!game.ID || this.state.leaveGame) {
      return <Redirect push to={'/Agentfall'} />
    }
    return (
      <Container style={{ padding: '10%', height: '100%' }}>
        <Segment padded={false}>
          <Label ribbon='right' color='green'>
          Connection ID:
            <Label.Detail>{game.ID}</Label.Detail>
          </Label>
          <Segment basic textAlign='center' padded>
            <Header as='h1'>{game.name} Lobby</Header>
            <Divider />
            <Header as='h3'>Players Joined</Header>
            <SemanticCard.Group centered itemsPerRow={people.length >= 6 ? 3 : 2} >
              {people}
            </SemanticCard.Group>
            <Segment basic >
              <Button color='green' content='Start' onClick={this.handleStart} size='big' />
              <Button color='green' content='Leave' onClick={this.handleLeave} size='big' />
            </Segment>
          </Segment>
        </Segment>
      </Container>
    )
  }
}

export default withRouter(Lobby)

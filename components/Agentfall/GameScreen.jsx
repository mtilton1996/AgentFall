import React, { Component } from 'react'
import { Grid, Container } from 'semantic-ui-react/dist/commonjs'
import { withRouter } from 'react-router-dom'
import Lobby from './Game/Lobby'
import Notepad from './Game/Notepad'

class GameScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      timer: '5:00' // seconds,
    }
  }

  render () {
    return (
      <Container style={{ paddingTop: '2%' }}>
        <Grid divided >
          <Grid.Row stretched>
            <Grid.Column width={4} >
              <Lobby />
            </Grid.Column>
            <Grid.Column width={12} >
              <Notepad />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    )
  }
}

export default withRouter(GameScreen)

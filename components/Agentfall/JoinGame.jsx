import React, { Component } from 'react'
import { Header, Segment, Divider, Form, Button, Container } from 'semantic-ui-react/dist/commonjs'
import { withRouter, Redirect } from 'react-router-dom'
import { AllContext } from '../contextStore'
import { joinGame } from '../../api/joinGame'
import { subscribeToGameJoined } from '../../api/subscribeToGameJoined'
import { unsubscribe } from '../../api/unsubscribe'

class JoinGame extends Component {
  constructor (props) {
    super(props)
    this.state = {
      nickname: '',
      gameID: '',
      readyToJoin: false,
      cancel: false
    }
    this.handleJoin = this.handleJoin.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  componentDidMount () {
    subscribeToGameJoined((userID) => {
      localStorage.setItem('user', JSON.stringify({
        ID: userID,
        nickname: this.state.nickname
      }))
      localStorage.setItem('game', JSON.stringify({
        ID: this.state.gameID
      }))
      this.setState({readyToJoin: true})
    })
  }

  componentWillUnmount () {
    unsubscribe(['gameJoined'])
  }

  onChange (event, data) {
    const name = event.target.name
    this.setState({[name]: data.value})
  }

  handleCancel () {
    this.setState({cancel: true})
  }

  handleJoin () {
    // TODO Check for user id
    joinGame({
      username: this.state.nickname,
      userId: null,
      gameId: this.state.gameID
    })
  }
  render () {
    let game = JSON.parse(localStorage.getItem('game'))
    if (this.state.readyToJoin && game.ID) {
      return <Redirect push to={'/Agentfall/lobby/' + game.ID} />
    }
    if (this.state.cancel) {
      return <Redirect push to={'/Agentfall/'} />
    }
    return (
      <Container style={{ padding: '10%', height: '100%' }}>
        <Segment padded='very' textAlign='center'>
          <Header as='h1'>Join Game</Header>
          <Divider />
          <Form>
            <Form.Input name='nickname' fluid label='Nickname' value={this.state.nickname} onChange={this.onChange} />
            <Form.Input name='gameID' fluid label='Connection ID' value={this.state.gameID} onChange={this.onChange} />
          </Form>
          <br />
          <Segment basic >
            <Button color='green' content='Join' onClick={this.handleJoin} size='big' />
            <Button color='green' content='Cancel' onClick={this.handleCancel} size='big' />
          </Segment>
        </Segment>
      </Container>
    )
  }
}

export default withRouter(JoinGame)

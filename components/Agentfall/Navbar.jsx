import React, { Component } from 'react'
import { Menu } from 'semantic-ui-react'

const navItems = ['Home', 'New Game', 'Join Game']

class Navbar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      active: 'menu'
    }
  }

  render () {
    return (
      <Menu fixed='top' inverted>
        <Menu.Item as='a' header>
                Agentfall
        </Menu.Item>
        {/* <Menu.Item key={c} name={c} active={activeA === c} color={c} onClick={this.handleAClick} /> */}
      </Menu>
    )
  }
}

export default Navbar

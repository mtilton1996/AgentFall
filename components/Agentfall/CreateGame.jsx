import React, { Component } from 'react'
import { Header, Button, Segment, Divider, Form, Container } from 'semantic-ui-react'
import { withRouter, Link, Redirect } from 'react-router-dom'

import LoadingScreen from './LoadingScreen'
import { createGame } from '../../api/createGame'
import { subscribeToGameCreated } from '../../api/subscribeToGameCreated'
import { unsubscribe } from '../../api/unsubscribe'

const timeOptions = [
  { text: '5:00', value: 300 },
  { text: '10:00', value: 600 },
  { text: '15:00', value: 900 }
]

class CreateGame extends Component {
  constructor (props) {
    super(props)
    this.state = {
      nickname: '',
      gameName: '',
      time: 300,
      isWaitingOnCreation: false,
      gameCreated: false
    }

    this.createGame = this.createGame.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  componentDidMount () {
    subscribeToGameCreated((gameID) => {
      let game = {
        name: this.state.gameName,
        ID: gameID
      }
      localStorage.setItem('game', JSON.stringify(game))
      this.setState({isWaitingOnCreation: false, gameCreated: true})
    })
  }

  componentWillUnmount () {
    unsubscribe(['gameCreated'])
  }

  onChange (event, data) {
    this.setState({[data.name]: data.value})
  }

  createGame () {
    createGame({
      nickname: this.state.nickname,
      time: this.state.time,
      GameName: this.state.gameName,
      theme: null
    })
    this.setState({isWaitingOnCreation: true})
  }
  render () {
    let game = JSON.parse(localStorage.getItem('game'))
    if (this.state.isWaitingOnCreation) {
      return <LoadingScreen text='Creating Game' />
    } else if (this.state.gameCreated && game.ID) {
      return <Redirect push to={'/Agentfall/lobby/' + game.ID} />
    } else {
      return (
        <Container style={{ padding: '10%', height: '100%' }}>
          <Segment padded='very' textAlign='center'>
            <Header as='h1'>Game Options</Header>
            <Divider />
            <Form>
              <Form.Input fluid label='Nickname' name='nickname' value={this.state.nickname} onChange={this.onChange} />
              <Form.Input fluid label='Game Name' name='gameName' value={this.state.gameName} onChange={this.onChange} />
              <Form.Dropdown search selection fluid label='Time' name='time' options={timeOptions} onChange={this.onChange} value={this.state.time} />
            </Form>
            <br />
            <Segment basic>
              <Button color='green' content='Create' onClick={this.createGame} size='big' />
              <Button color='green' content='Cancel' as={Link} to='/Agentfall' size='big' />
            </Segment>
          </Segment>
        </Container>
      )
    }
  }
}

export default withRouter(CreateGame)

import React, { Component } from 'react'
import { Segment, Container, Dimmer, Loader } from 'semantic-ui-react/dist/commonjs'

/**
 * Simple loading screen
 */
class LoadingScreen extends Component {
  render () {
    return (
      <Container style={{ padding: '10%', height: '100%' }}>
        <Segment padded='very' textAlign='center' style={{ padding: '10%', height: '80%' }}>
          <Dimmer active inverted>
            <Loader size='massive'>{this.props.text}</Loader>
          </Dimmer>
        </Segment>
      </Container>
    )
  }
}

LoadingScreen.defaultProps = {
  text: 'Loading...'
}

export default LoadingScreen

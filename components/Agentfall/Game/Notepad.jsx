import React, { Component } from 'react'
import { Header, Segment, Label, Sidebar, Menu, Icon, Button, Card as SemanticCard } from 'semantic-ui-react/dist/commonjs'
import Card from '../Card'

class Notepad extends Component {
  constructor (props) {
    super(props)
    this.state = {
      visible: false,
      active: false,
      locations: [
        {name: 'nam'},
        {name: 'libero '},
        {name: 'justo '},
        {name: 'laoreet '},
        {name: 'site '},
        {name: 'ameto '},
        {name: 'cursus '},
        {name: 'sita '},
        {name: 'ameat '},
        {name: 'dictum '},
        {name: 'sit '},
        {name: 'amet '},
        {name: 'justo'},
        {name: 'donec '},
        {name: 'enim '},
        {name: 'diam '},
        {name: 'vulputate '},
        {name: 'ut'},
        {name: 'pharetra'},
        {name: 'sit'}
      ]
    }
  }

  componentDidMount () {
    // TODO retrieve locations from server
  }
  render () {
    const { visible } = this.state
    const { active } = this.state
    let locations = this.state.locations.map(function (location, i) {
      return <Card {...location} isPlaying key={location.name} />
    })
    let game = localStorage.getItem('game')
    return (
      <Sidebar.Pushable as={Segment}>
        <Sidebar as={Menu} animation='push' direction='top' visible={visible} color='green'>
          <Menu.Item name='home'>
            <Button toggle active={active} inverted color='green' onClick={() => this.setState({ active: !this.state.active })}>
              Toggle
            </Button>
          </Menu.Item>
          <Menu.Item name='gamepad'>
            <Icon name='gamepad' />
              Games
          </Menu.Item>
          <Menu.Item name='camera'>
            <Icon name='camera' />
              Channels
          </Menu.Item>
          <Menu.Menu position='right'>
            <Menu.Item name='cid'>
              CID: {game.ID}
            </Menu.Item>
          </Menu.Menu>
        </Sidebar>
        <Sidebar.Pusher>
          <Segment basic>
            <Label size='large' as='a' color='green' corner='right' onClick={() => this.setState({ visible: !this.state.visible })} icon='arrow down' />
            <Header as='h3'>Locations</Header>
            <SemanticCard.Group centered itemsPerRow={this.state.locations.length >= 9 ? 4 : 3} >
              {locations}
            </SemanticCard.Group>
          </Segment>
        </Sidebar.Pusher>
        {/* TODO Add clear button for colors */}
      </Sidebar.Pushable>
    )
  }
}

Notepad.defaultProps = {
  gameID: null
}

export default Notepad

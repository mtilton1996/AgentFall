import React, { Component } from 'react'
import { Card as SemanticCard, Segment, Label, Progress, List, Divider } from 'semantic-ui-react/dist/commonjs'
import Card from '../Card'

class Lobby extends Component {
  constructor (props) {
    super(props)
    this.state = {

      people: [
        {name: 'John'},
        {name: 'Jenkins'},
        {name: 'Jake'},
        {name: 'Jim'}
      ]
    }
  }

  render () {
    let people = this.state.people.map(function (person, i) {
      return <Card {...person} isPlaying key={person.name} />
    })
    let percent = 50
    let gameName = localStorage.getItem('gameName')
    let user = localStorage.getItem('user')
    return (<Segment.Group>
      <Segment>
        <List>
          <List.Item>
            <List.Header>{gameName}</List.Header>
          </List.Item>
          <List.Item>
            <List.Header>{user.nickname}</List.Header>
          </List.Item>
          <Divider />
          <List.Item>
            <Label size='small'>Location</Label>{' '}Japan
          </List.Item>
          <List.Item>
            <Label size='small'>Role</Label>{' '} Not Agent
          </List.Item>
        </List>
      </Segment>
      <Segment>
        <Progress attached='top' color={percent >= 75 ? 'red' : 'green'} size='large' inverted percent={percent} active />
        <Label basic color={percent >= 75 ? 'red' : 'green'} pointing='right'> Timer
        </Label>{' ' + this.state.timer}
      </Segment>
      <Segment>
        <Progress attached='top' color={percent >= 75 ? 'red' : 'green'} inverted percent={percent} active />
        <SemanticCard.Group centered itemsPerRow={1} >
          {people}
        </SemanticCard.Group>
      </Segment>
    </Segment.Group>
    )
  }
}

Lobby.defaultProps = {
  gameName: '',
  gameID: null
}

export default Lobby

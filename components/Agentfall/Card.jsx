import React, { Component } from 'react'
import { Card as SemanticCard, Label } from 'semantic-ui-react/dist/commonjs'

const colors = [ 'grey', 'yellow', 'red', 'green' ]

class Card extends Component {
  constructor (props) {
    super(props)
    this.state = {
      color: this.props.color
    }
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick (event) {
    if (this.props.isPlaying) {
      this.setState(state => {
        return { color: colors[colors.indexOf(state.color) + 1] }
      })
    }
    this.props.onClick(this.props.name)
  }

  render () {
    return (
      <SemanticCard
        link={this.props.isPlaying || this.props.isVoting}
        onClick={this.props.isPlaying ? (event) => this.handleClick(event) : null}
        color={this.props.isPlaying || this.props.isVoting ? this.state.color : null} >
        <SemanticCard.Content>
          <SemanticCard.Header>
            {this.props.name}
            {(this.props.isPlaying || this.props.isSelected) && <Label size='mini' color={this.state.color} corner='right' />}
          </SemanticCard.Header>
        </SemanticCard.Content>
      </SemanticCard>
    )
  }
}

Card.defaultProps = {
  isPlaying: false,
  isVoting: false,
  color: 'grey',
  isSelected: false,
  onClick: null,
  name: null
}

export default Card

import React, { Component } from 'react'
import { Header, Button, Divider, Segment, Container } from 'semantic-ui-react/dist/commonjs'
import { withRouter, Link } from 'react-router-dom'
import { withCookies, Cookies } from 'react-cookie'
import { instanceOf } from 'prop-types'
import { AllContext } from '../contextStore'

import { subscribeToTimer } from '../../api/subscribeToTimer'
class Menu extends Component {
  constructor (props) {
    super(props)

    this.state = {
      timestamp: 'None'
    }
  }

  clearStorage () {
    localStorage.clear()
  }

  render () {
    console.log('timestamp: ', this.state.timestamp)
    return (
      <Container style={{ padding: '10%', height: '100%' }}>
        <Segment textAlign='center' padded='very'>
          <Header as='h1'>Agentfall</Header>
          <Divider />
          <Button size='massive' color='green' circular content='New Game' as={Link} to='/Agentfall/setup' />
          <Button size='massive' color='green' circular content='Join Game' as={Link} to='/Agentfall/join' />
          <br />
          <br />
          {/* <Button as={Link} to="/Agentfall/create-theme" content='Create Theme' /> */}
          <Button onClick={this.clearStorage.bind(this)} content='Clear Data' />
        </Segment>
      </Container>
    )
  }
}

export default withRouter(Menu)

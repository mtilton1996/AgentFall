import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import { Message } from 'semantic-ui-react/dist/commonjs'

import Menu from './Menu'
import CreateGame from './CreateGame'
import Lobby from './Lobby'
import GameScreen from './GameScreen'
import JoinGame from './JoinGame'
import Voting from './Voting'
import { subscribeToError } from '../../api/subscribeToError'
import { AllContext } from '../contextStore'

class Controller extends Component {
  constructor (props) {
    super(props)

    this.state = {
      // gameID: null,
      // user: {
      //   nickname: '',
      //   ID: null
      // },
      error: null
    }
  }

  // updateUser = (nickname, ID) => {
  //   this.setState(state => ({
  //     user:{
  //       nickname: nickname,
  //       ID: ID
  //     }
  //   }))
  // }

  // updateGameID = (ID) => {
  //   this.setState({ gameID:ID })
  // }
  componentDidMount () {
    subscribeToError((error) => {
      this.setState({error: error})
    })
  }
  handleDismiss = () => {
    this.setState({ error: null })
  }

  render () {
    return (
      <AllContext.Provider value={this.state}>
        { this.state.error && <Message negative onDismiss={this.handleDismiss}>
          <Message.Header>{this.state.error}</Message.Header>
        </Message> }
        <Switch>
          <Route exact path='/Agentfall' component={Menu} />
          <Route path='/Agentfall/setup' component={CreateGame} />
          <Route exact path='/Agentfall/lobby/:id' component={Lobby} />
          <Route exact path='/Agentfall/game/:gameId' component={GameScreen} />
          <Route path='/Agentfall/voting' component={Voting} />
          <Route path='/Agentfall/join' component={JoinGame} />
        </Switch>
      </AllContext.Provider >
    )
  }
}

export default Controller

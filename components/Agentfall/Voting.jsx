import React, { Component } from 'react'
import { Header, Segment, Container, Card as SemanticCard, Divider } from 'semantic-ui-react/dist/commonjs'
import { withRouter } from 'react-router-dom'
import { withCookies } from 'react-cookie'
import { AllContext } from '../contextStore'
import Lobby from './Lobby'
import Card from './Card'

class Voting extends Component {
  constructor (props) {
    super(props)
    this.state = {

      people: [
        {name: 'John'},
        {name: 'Jenkins'},
        {name: 'Jake'},
        {name: 'Jim'}
      ],
      selected: null
    }
    this.handleClick = this.handleClick.bind(this)
  }
  handleClick (name) {
    console.log('name', name)
    this.setState({
      selected: name
    })
  }

  render () {
    let people = this.state.people.map((person, i) => {
      return <Card {...person} isVoting isSelected={(person.name === this.state.selected)} color='green' key={person.name} name={person.name} onClick={this.handleClick} />
    })
    return (
      <Container style={{ padding: '10%', height: '100%' }}>
        <Segment textAlign='center' padded='very'>
          <Header as='h1'>Voting</Header>
          <SemanticCard.Group centered itemsPerRow={2} >
            {people}
          </SemanticCard.Group>
        </Segment>

      </Container>
    )
  }
}

export default withCookies(withRouter(Voting))

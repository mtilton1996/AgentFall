import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import { CookiesProvider } from 'react-cookie'

import 'semantic-ui-css/semantic.css'
import '../custom.css'

import Info from './Info'
import Agentfall from './Agentfall/Controller'

// import ws from '../sockette'
class Main extends Component {
  render () {
    return (
      <CookiesProvider>
        <Router>
          <Switch>
            <Route exact path='/' component={Info} />
            <Route path='/Agentfall' component={Agentfall} />
            <Route path='*' component={allOther} />
          </Switch>
        </Router>
      </CookiesProvider>
    )
  }
}

function allOther () {
  return <Redirect to='/' />
}

ReactDOM.render(<Main />, document.getElementById('root'))

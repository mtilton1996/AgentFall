import React, { Component } from 'react'
import { Button } from 'semantic-ui-react/dist/commonjs'
import {Link} from 'react-router-dom'

class Info extends Component {
  render () {
    return (
      <div >
        Welcome!
        <br />
        <Button content='Agentfall' as={Link} to='/Agentfall' />
      </div>
    )
  }
}

export default Info

import React from 'react'

export const AllContext = React.createContext({
  gameID: null,
  user: {
    nickname: null,
    ID: null
  },
  updateUser: () => {},
  updateGameID: () => {}
})

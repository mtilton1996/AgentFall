const path = require('path')

module.exports = {
  entry: './components/Main.jsx',
  output: {
    path: path.resolve(__dirname, 'public/'),
    filename: 'javascripts/bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: [/\.eot$/, /\.ttf$/, /\.svg$/, /\.woff$/, /\.woff2$/],
        loader: require.resolve('file-loader'),
        options: {
          name: '/static/media/[name].[hash:8].[ext]'
        }
      },
      {
        test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
        loader: require.resolve('url-loader'),
        options: {
          limit: 10000,
          name: '/static/media/[name].[hash:8].[ext]'
        }
      }
    ]
  },
  resolve: {
    extensions: [ '.js', '.jsx' ]
  },
  watchOptions: {
    ignored: /node_modules/
  }
}

/**
 * Generates a random id
 */
function getNewRandomId () {
    var randomString = ''
    var choices = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  
    for (var i = 0; i < 10; i++) {
      randomString += choices.charAt(Math.floor(Math.random() * choices.length))
    }
  
    return randomString
  }

  module.exports = getNewRandomId
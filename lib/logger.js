var winston = require('winston')
var log = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
        winston.format.json(),
        winston.format.timestamp(),
        winston.format.prettyPrint()
    ),
    transports: [
      new winston.transports.File({ filename: 'error.log', level: 'error' }),
      new winston.transports.File({ filename: 'warn.log', level: 'warn' }),
      new winston.transports.File({ filename: 'info.log', level: 'info' })
    ]
  })
  
  if (process.env.NODE_ENV !== 'production') {
    log.add(new winston.transports.Console({
      format: winston.format.simple()
    }));
  }

module.exports = log